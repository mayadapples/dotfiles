# Created 12-5-2021.
function add_to_path --description 'Persistently adds segment to PATH variable.'
    set --universal fish_user_paths $fish_user_paths $argv
end
