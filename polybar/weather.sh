#!/bin/sh

weather=$(curl -Ss 'https://wttr.in?Q&0T&m' | cut -c 16- | head -2 | xargs echo)
echo "$weather"
