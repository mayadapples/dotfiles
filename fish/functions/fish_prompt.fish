

function fish_prompt -d "Write out prompt."
    set -l last_status $status

    set -l color_cwd $fish_color_cwd
    set -l suffix '$'
    if functions -q fish_is_root_user; and fish_is_root_user
        if set -q fish_color_cwd_root
            set color_cwd $fish_color_cwd_root
        end
        set suffix '#'
    end

    printf '%s[%s%s%s%s%s@%s%s%s%s %s%s%s]%s%s%s ' \
        (set_color magenta) (set_color normal) \
        (set_color blue) $USER (set_color normal) \
        (set_color red) (set_color normal) \
        (set_color yellow) $hostname (set_color normal) \
        (set_color $color_cwd) (prompt_pwd) \
        (set_color cyan) (set_color normal) \
        (set_color normal) $suffix
end
