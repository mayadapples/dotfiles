function set_background --description 'Change the background and set it permanently.'
    cp $argv '/home/theo/Media/Wallpaper/background-0.jpg'
    feh --bg-fill $argv
    i3-msg restart
end
